﻿using Juego1;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace juegomemory
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture("el-GR");

        //ICommand para usar en el binding de los botones
        public ICommand BotoCarta1 { get; set; }
        public ICommand BotoCarta2 { get; set; }
        public ICommand BotoCarta3 { get; set; }
        public ICommand BotoCarta4 { get; set; }
        public ICommand BotoCarta5 { get; set; }
        public ICommand BotoCarta6 { get; set; }
        public ICommand BotoCarta7 { get; set; }
        public ICommand BotoCarta8 { get; set; }
        public ICommand BotoCarta9 { get; set; }
        public ICommand BotoCarta10 { get; set; }
        public ICommand BotoCarta11 { get; set; }
        public ICommand BotoCarta12 { get; set; }
        public ICommand BotoReiniciar { get; set; }

        //Declaración de variables booleanas
        public Boolean estatcarta1 = true;
        public Boolean estatcarta2 = true;
        public Boolean estatcarta3 = true;
        public Boolean estatcarta4 = true;
        public Boolean estatcarta5 = true;
        public Boolean estatcarta6 = true;
        public Boolean estatcarta7 = true;
        public Boolean estatcarta8 = true;
        public Boolean estatcarta9 = true;
        public Boolean estatcarta10 = true;
        public Boolean estatcarta11 = true;
        public Boolean estatcarta12 = true;
        public Boolean activarCarta1 = true;
        public Boolean activarCarta2 = true;
        public Boolean activarCarta3 = true;
        public Boolean activarCarta4 = true;
        public Boolean activarCarta5 = true;
        public Boolean activarCarta6 = true;
        public Boolean activarCarta7 = true;
        public Boolean activarCarta8 = true;
        public Boolean activarCarta9 = true;
        public Boolean activarCarta10 = true;
        public Boolean activarCarta11 = true;
        public Boolean activarCarta12 = true;
        public Boolean cartaVisible1 = false;
        public Boolean cartaVisible2 = false;
        public Boolean cartaVisible3 = false;
        public Boolean cartaVisible4 = false;
        public Boolean flag = true;
        public Boolean visibleReiniciar = false;
        
        //Declaración de Arrays
        String [] ArrayCartes = new String[12];
        String [] ArrayFotos = new String[19];

        //Declaración de variables int i String
        public int nivel = 4;
        public int marcado = 0;
        public int numeroClick = 0;
        public int contgirades = 0;
        public int numrandom = 0;
        public int posrandom = 0;
        public int posrandom2 = 0;
        public int puntuacio = 0;
        public int puntuacioflag = 0;
        public String flagfotos = "null";
        public String valorar1 = "null";
        public String valorar2 = "null";
        public String textMostrar = "MEMORY";
        public String imatgeCarta1 = "cartareves.png";
        public String imatgeCarta2 = "cartareves.png";
        public String imatgeCarta3 = "cartareves.png";
        public String imatgeCarta4 = "cartareves.png";
        public String imatgeCarta5 = "cartareves.png";
        public String imatgeCarta6 = "cartareves.png";
        public String imatgeCarta7 = "cartareves.png";
        public String imatgeCarta8 = "cartareves.png";
        public String imatgeCarta9 = "cartareves.png";
        public String imatgeCarta10 = "cartareves.png";
        public String imatgeCarta11 = "cartareves.png";
        public String imatgeCarta12 = "cartareves.png";
        public String imatgeRandom1 = "cartareves.png";
        public String imatgeRandom2 = "cartareves.png";
        public String imatgeRandom3 = "cartareves.png";
        public String imatgeRandom4 = "cartareves.png";

        //Declaración de las variables para bindear
        public int Puntuacio
        {
            set
            {
                if (puntuacio != value)
                {
                    puntuacio = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Puntuacio"));
                    }
                }
            }
            get { return puntuacio; }
        }
        public String ImatgeCarta1
        {
            set
            {
                if (imatgeCarta1 != value)
                {
                    imatgeCarta1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeCarta1"));
                    }
                }
            }
            get { return imatgeCarta1; }
        }
        public String ImatgeCarta2
        {
            set
            {
                if (imatgeCarta2 != value)
                {
                    imatgeCarta2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeCarta2"));
                    }
                }
            }
            get { return imatgeCarta2; }
        }
        public String ImatgeCarta3
        {
            set
            {
                if (imatgeCarta3 != value)
                {
                    imatgeCarta3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeCarta3"));
                    }
                }
            }
            get { return imatgeCarta3; }
        }
        public String ImatgeCarta4
        {
            set
            {
                if (imatgeCarta4 != value)
                {
                    imatgeCarta4 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeCarta4"));
                    }
                }
            }
            get { return imatgeCarta4; }
        }
        public String ImatgeCarta5
        {
            set
            {
                if (imatgeCarta5 != value)
                {
                    imatgeCarta5 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeCarta5"));
                    }
                }
            }
            get { return imatgeCarta5; }
        }
        public String ImatgeCarta6
        {
            set
            {
                if (imatgeCarta6 != value)
                {
                    imatgeCarta6 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeCarta6"));
                    }
                }
            }
            get { return imatgeCarta6; }
        }
        public String ImatgeCarta7
        {
            set
            {
                if (imatgeCarta7 != value)
                {
                    imatgeCarta7 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeCarta7"));
                    }
                }
            }
            get { return imatgeCarta7; }
        }
        public String ImatgeCarta8
        {
            set
            {
                if (imatgeCarta8 != value)
                {
                    imatgeCarta8 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeCarta8"));
                    }
                }
            }
            get { return imatgeCarta8; }
        }
        public String ImatgeCarta9
        {
            set
            {
                if (imatgeCarta9 != value)
                {
                    imatgeCarta9 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeCarta9"));
                    }
                }
            }
            get { return imatgeCarta9; }
        }
        public String ImatgeCarta10
        {
            set
            {
                if (imatgeCarta10 != value)
                {
                    imatgeCarta10 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeCarta10"));
                    }
                }
            }
            get { return imatgeCarta10; }
        }
        public String ImatgeCarta11
        {
            set
            {
                if (imatgeCarta11 != value)
                {
                    imatgeCarta11 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeCarta11"));
                    }
                }
            }
            get { return imatgeCarta11; }
        }
        public String ImatgeCarta12
        {
            set
            {
                if (imatgeCarta12 != value)
                {
                    imatgeCarta12 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImatgeCarta12"));
                    }
                }
            }
            get { return imatgeCarta12; }
        }
        public String TextMostrar
        {
            set
            {
                if (textMostrar != value)
                {
                    textMostrar = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TextMostrar"));
                    }
                }
            }
            get { return textMostrar; }
        }
        public Boolean ActivarCarta1
        {
            set
            {
                if (activarCarta1 != value)
                {
                    activarCarta1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ActivarCarta1"));
                    }
                }
            }
            get { return activarCarta1; }
        }
        public Boolean ActivarCarta2
        {
            set
            {
                if (activarCarta2 != value)
                {
                    activarCarta2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ActivarCarta2"));
                    }
                }
            }
            get { return activarCarta2; }
        }
        public Boolean ActivarCarta3
        {
            set
            {
                if (activarCarta3 != value)
                {
                    activarCarta3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ActivarCarta3"));
                    }
                }
            }
            get { return activarCarta3; }
        }
        public Boolean ActivarCarta4
        {
            set
            {
                if (activarCarta4 != value)
                {
                    activarCarta4 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ActivarCarta4"));
                    }
                }
            }
            get { return activarCarta4; }
        }
        public Boolean ActivarCarta5
        {
            set
            {
                if (activarCarta5 != value)
                {
                    activarCarta5 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ActivarCarta5"));
                    }
                }
            }
            get { return activarCarta5; }
        }
        public Boolean ActivarCarta6
        {
            set
            {
                if (activarCarta6 != value)
                {
                    activarCarta6 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ActivarCarta6"));
                    }
                }
            }
            get { return activarCarta6; }
        }
        public Boolean ActivarCarta7
        {
            set
            {
                if (activarCarta7 != value)
                {
                    activarCarta7 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ActivarCarta7"));
                    }
                }
            }
            get { return activarCarta7; }
        }
        public Boolean ActivarCarta8
        {
            set
            {
                if (activarCarta8 != value)
                {
                    activarCarta8 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ActivarCarta8"));
                    }
                }
            }
            get { return activarCarta8; }
        }
        public Boolean ActivarCarta9
        {
            set
            {
                if (activarCarta9 != value)
                {
                    activarCarta9 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ActivarCarta9"));
                    }
                }
            }
            get { return activarCarta9; }
        }
        public Boolean ActivarCarta10
        {
            set
            {
                if (activarCarta10 != value)
                {
                    activarCarta10 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ActivarCarta10"));
                    }
                }
            }
            get { return activarCarta10; }
        }
        public Boolean ActivarCarta11
        {
            set
            {
                if (activarCarta11 != value)
                {
                    activarCarta11 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ActivarCarta11"));
                    }
                }
            }
            get { return activarCarta11; }
        }
        public Boolean ActivarCarta12
        {
            set
            {
                if (activarCarta12 != value)
                {
                    activarCarta12 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ActivarCarta12"));
                    }
                }
            }
            get { return activarCarta12; }
        }
        public Boolean CartaVisible1
        {
            set
            {
                if (cartaVisible1 != value)
                {
                    cartaVisible1 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("CartaVisible1"));
                    }
                }
            }
            get { return cartaVisible1; }
        }
        public Boolean CartaVisible2
        {
            set
            {
                if (cartaVisible2 != value)
                {
                    cartaVisible2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("CartaVisible2"));
                    }
                }
            }
            get { return cartaVisible2; }
        }
        public Boolean CartaVisible3
        {
            set
            {
                if (cartaVisible3 != value)
                {
                    cartaVisible3 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("CartaVisible3"));
                    }
                }
            }
            get { return cartaVisible3; }
        }
        public Boolean CartaVisible4
        {
            set
            {
                if (cartaVisible4 != value)
                {
                    cartaVisible4 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("CartaVisible4"));
                    }
                }
            }
            get { return cartaVisible4; }
        }
        public Boolean VisibleReiniciar
        {
            set
            {
                if (visibleReiniciar != value)
                {
                    visibleReiniciar = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("VisibleReiniciar"));
                    }
                }
            }
            get { return visibleReiniciar; }
        }
        public MainPageViewModel()
        { //Aqui aparecen los command (botones) y el array de imagenes que se inicializa
            BotoCarta1 = new Command(Carta1);
            BotoCarta2 = new Command(Carta2);
            BotoCarta3 = new Command(Carta3);
            BotoCarta4 = new Command(Carta4);
            BotoCarta5 = new Command(Carta5);
            BotoCarta6 = new Command(Carta6);
            BotoCarta7 = new Command(Carta7);
            BotoCarta8 = new Command(Carta8);
            BotoCarta9 = new Command(Carta9);
            BotoCarta10 = new Command(Carta10);
            BotoCarta11 = new Command(Carta11);
            BotoCarta12 = new Command(Carta12);
            BotoReiniciar = new Command(Reiniciar);
            ArrayFotos[0] = "barril.png";
            ArrayFotos[1] = "burger.png";
            ArrayFotos[2] = "campana.png";
            ArrayFotos[3] = "corazon.png";
            ArrayFotos[4] = "diamante.png";
            ArrayFotos[5] = "dinero.png";
            ArrayFotos[6] = "moneda.png";
            ArrayFotos[7] = "quadradoamarillo.png";
            ArrayFotos[8] = "quadradogris.png";
            ArrayFotos[9] = "quadradorojo.png";
            ArrayFotos[10] = "quadradoverde.png";
            ArrayFotos[11] = "reloj.png";
            ArrayFotos[12] = "cireres.png";
            ArrayFotos[13] = "fresa.png";
            ArrayFotos[14] = "limon.png";
            ArrayFotos[15] = "manzana.png";
            ArrayFotos[16] = "naranja.png";
            ArrayFotos[17] = "sandia.png";
            ArrayFotos[18] = "uvas.png";
            //LLamamos a la funcion RandomCartes() para asignar cada imagen a una carta de forma aleatoria
            RandomCartes();
        }

        //Funciones que se ejecutan al hacer click a cada carta
        public void Carta1()
        {
            numeroClick++; //Se suma uno al numero de clicks
            
            ImatgeCarta1 = ArrayCartes[0]; 
            estatcarta1 = true;

            ComprovarCartes(ImatgeCarta1); //Entra en la funcion de comprovar cartas
        } 
        public void Carta2()
        {
            numeroClick++;
            
            ImatgeCarta2 = ArrayCartes[1];
            estatcarta2 = true;

            ComprovarCartes(ImatgeCarta2);
        }
        public void Carta3()
        {
            numeroClick++;
            
            ImatgeCarta3 = ArrayCartes[2];
            estatcarta3 = true;

            ComprovarCartes(ImatgeCarta3);
        }
        public void Carta4()
        {
            numeroClick++;
           
            ImatgeCarta4 = ArrayCartes[3];
            estatcarta4 = true;

            ComprovarCartes(ImatgeCarta4);
        }
        public void Carta5()
        {
            numeroClick++;
            
            ImatgeCarta5 = ArrayCartes[4];
            estatcarta5 = true;

            ComprovarCartes(ImatgeCarta5);
        }
        public void Carta6()
        {
            numeroClick++;

            ImatgeCarta6 = ArrayCartes[5];
            estatcarta6 = true;

            ComprovarCartes(ImatgeCarta6);
        }
        public void Carta7()
        {
            numeroClick++;
           
            ImatgeCarta7 = ArrayCartes[6];
            estatcarta7 = true;

            ComprovarCartes(ImatgeCarta7);
        }
        public void Carta8()
        {
            numeroClick++;
            
            ImatgeCarta8 = ArrayCartes[7];
            estatcarta8 = true;

            ComprovarCartes(ImatgeCarta8);

        }
        public void Carta9()
        {
            numeroClick++;

            ImatgeCarta9 = ArrayCartes[8];
            estatcarta9 = true;

            ComprovarCartes(ImatgeCarta9);

        }
        public void Carta10()
        {
            numeroClick++;

            ImatgeCarta10 = ArrayCartes[9];
            estatcarta10 = true;

            ComprovarCartes(ImatgeCarta10);

        }
        public void Carta11()
        {
            numeroClick++;

            ImatgeCarta11 = ArrayCartes[10];
            estatcarta11 = true;

            ComprovarCartes(ImatgeCarta11);

        }
        public void Carta12()
        {
            numeroClick++;

            ImatgeCarta12 = ArrayCartes[11];
            estatcarta12 = true;

            ComprovarCartes(ImatgeCarta12);

        }
        public void ComprovarCartes(String CartaEntrada)
        {
            //En la funcion entra el src (string) de la carta
            //Con esta condicion y el flag se guarda a otra variable para poder comprobar cada dos veces
            if (flag == true)
            {
                valorar1 = CartaEntrada;
                flag = false;
            }
            else
            {
                valorar2 = CartaEntrada;
                flag = true;
            }
            //Cuando el numero de clicks sea igual a dos, las cartas se desactivan (no hacen nada si las tocas)
            if (numeroClick == 2)
            {
                ActivarCarta1 = false;
                ActivarCarta2 = false;
                ActivarCarta3 = false;
                ActivarCarta4 = false;
                ActivarCarta5 = false;
                ActivarCarta6 = false;
                ActivarCarta7 = false;
                ActivarCarta8 = false;
                ActivarCarta9 = false;
                ActivarCarta10 = false;
                ActivarCarta11 = false;
                ActivarCarta12 = false;

                //Si las cartas son diferentes
                if (valorar1 != valorar2)
                {
                    TextMostrar = "OH! VUELVE A INTENTARLO!";
                    Unsegon(); //Pausa de un segundo
                    contgirades = 0; //No se ha girado ninguna carta

                    //Nunca el numero de puntos será inferior a zero
                    if (puntuacioflag > 0)
                    { 
                        //Resta mil puntos porque se ha fallado
                        puntuacioflag = puntuacioflag - 1000;
                        Puntuacio = puntuacioflag;
                    }
                    
                }
                else
                {
                    //Si las cartas son iguales
                    TextMostrar = "QUE BUENA!";
                    contgirades++;
                    //Suma mil puntos porque son iguales
                    puntuacioflag = puntuacioflag + 1000;
                    Puntuacio = puntuacioflag;
                    //Se activan todas las cartas
                    ActivarCarta1 = true;
                    ActivarCarta2 = true;
                    ActivarCarta3 = true;
                    ActivarCarta4 = true;
                    ActivarCarta5 = true;
                    ActivarCarta6 = true;
                    ActivarCarta7 = true;
                    ActivarCarta8 = true;
                    ActivarCarta9 = true;
                    ActivarCarta10 = true;
                    ActivarCarta11 = true;
                    ActivarCarta12 = true;
                }
                numeroClick = 0; //Se reinicializan los clicks
            }

            //Nivel superado
            if (contgirades == nivel) {
                //Cuando el numero de cartas giradas es igual al numero de nivel
                TextMostrar = "NIVEL SUPERADO!";
                //Se activa el boton de reiniciar
                VisibleReiniciar = true;
                //Se desactivan todas las cartas (no hacen nada si las tocas)
                ActivarCarta1 = false;
                ActivarCarta2 = false;
                ActivarCarta3 = false;
                ActivarCarta4 = false;
                ActivarCarta5 = false;
                ActivarCarta6 = false;
                ActivarCarta7 = false;
                ActivarCarta8 = false;
                ActivarCarta9 = false;
                ActivarCarta10 = false;
                ActivarCarta11 = false;
                ActivarCarta12 = false;
            }

        }
        public async Task Unsegon()
        {
            //Función para hacer una pausa de un segundo
            await Task.Delay(1000);
            //Se vuelven a poner todas las cartas giradas como al principio
            ImatgeCarta1 = "cartareves.png";
            ImatgeCarta2 = "cartareves.png";
            ImatgeCarta3 = "cartareves.png";
            ImatgeCarta4 = "cartareves.png";
            ImatgeCarta5 = "cartareves.png";
            ImatgeCarta6 = "cartareves.png";
            ImatgeCarta7 = "cartareves.png";
            ImatgeCarta8 = "cartareves.png";
            ImatgeCarta9 = "cartareves.png";
            ImatgeCarta10 = "cartareves.png";
            ImatgeCarta11 = "cartareves.png";
            ImatgeCarta12 = "cartareves.png";
            //Se activan las cartas
            ActivarCarta1 = true;
            ActivarCarta2 = true;
            ActivarCarta3 = true;
            ActivarCarta4 = true;
            ActivarCarta5 = true;
            ActivarCarta6 = true;
            ActivarCarta7 = true;
            ActivarCarta8 = true;
            ActivarCarta9 = true;
            ActivarCarta10 = true;
            ActivarCarta11 = true;
            ActivarCarta12 = true;
        }
        public void RandomCartes()
        {
            Random rnd = new Random();
            
            //Random para asignar un numero a cada carta
            List<int> listNumbers = new List<int>();
            int number;
            for (int i = 0; i < 8; i++)
            {
                do
                {
                    number = rnd.Next(8);
                } while (listNumbers.Contains(number));
                listNumbers.Add(number);
            }
            //Random para asignar una imagen a cada carta
            List<int> listFotos = new List<int>();
            int fotos;
            for (int i = 0; i < 19; i++)
            {
                do
                {
                    fotos = rnd.Next(19);
                } while (listFotos.Contains(fotos));
                listFotos.Add(fotos);
            }

            //Random para hacer dos cartas iguales
            for (int i = 0; i < 8; i = i + 2)
            {
                numrandom = rnd.Next(19);

                ArrayCartes[listNumbers[i]] = ArrayFotos[listFotos[i]];
                ArrayCartes[listNumbers[i+1]] = ArrayFotos[listFotos[i]];
            }

        }
        public void RandomCartes2()
        {
            Random rnd = new Random();

            //Random para asignar un numero a cada carta
            List<int> listNumbers = new List<int>();
            int number;
            for (int i = 0; i < 12; i++)
            {
                do
                {
                    number = rnd.Next(12);
                } while (listNumbers.Contains(number));
                listNumbers.Add(number);
            }

            //Random para asignar una imagen a cada carta
            List<int> listFotos = new List<int>();
            int fotos;
            for (int i = 0; i < 19; i++)
            {
                do
                {
                    fotos = rnd.Next(19);
                } while (listFotos.Contains(fotos));
                listFotos.Add(fotos);
            }

            //Random para hacer dos cartas iguales
            for (int i = 0; i < 12; i = i + 2)
            {
                numrandom = rnd.Next(19);

                ArrayCartes[listNumbers[i]] = ArrayFotos[listFotos[i]];
                ArrayCartes[listNumbers[i + 1]] = ArrayFotos[listFotos[i]];
            }

        }
        public void Reiniciar()
        {
            //Funcion que se ejecuta al hacer click en el boton de reiniciar
            TextMostrar = "MEMORY";
            //Se vuelven a poner todas las cartas giradas como al principio
            ImatgeCarta1 = "cartareves.png";
            ImatgeCarta2 = "cartareves.png";
            ImatgeCarta3 = "cartareves.png";
            ImatgeCarta4 = "cartareves.png";
            ImatgeCarta5 = "cartareves.png";
            ImatgeCarta6 = "cartareves.png";
            ImatgeCarta7 = "cartareves.png";
            ImatgeCarta8 = "cartareves.png";
            ImatgeCarta9 = "cartareves.png";
            ImatgeCarta10 = "cartareves.png";
            ImatgeCarta11 = "cartareves.png";
            ImatgeCarta12 = "cartareves.png";
            //Se vuelven a activar todas las cartas
            ActivarCarta1 = true;
            ActivarCarta2 = true;
            ActivarCarta3 = true;
            ActivarCarta4 = true;
            ActivarCarta5 = true;
            ActivarCarta6 = true;
            ActivarCarta7 = true;
            ActivarCarta8 = true;
            ActivarCarta9 = true;
            ActivarCarta10 = true;
            ActivarCarta11 = true;
            ActivarCarta12 = true;
            CartaVisible1 = true;
            CartaVisible2 = true;
            CartaVisible3 = true;
            CartaVisible4 = true;
            //Se oculta el boton de reiniciar
            VisibleReiniciar = false;
            //Se reinician variables contador
            numeroClick = 0;
            contgirades = 0;
            //Se aumenta al segundo nivel
            nivel = 6;
            flag = true;
            //Entra en la funcion del segundo nivel
            RandomCartes2();

        }
    }
}